@echo off
REM 日計担当者連携バッチ
REM 本ファイルと同じフォルダにDailyAccount.ps1ファイルを格納してください

SET /P ret="日計担当者データを連携します。よろしいですか？(y/n)"
IF "%ret%" == "y" (

powershell -Command ".\DailyAccount.ps1"

echo 処理が終了しました
pause
)

