<# 日計データ管理バッチ
 Oracleから出力された最新の日計データのcsvにヘッダを付けSalesforceにinsertしSalesforceから古い日計データをdeleteする。
#>
# 現在の日付を取得
$day = (Get-Date).ToString("yyyyMMdd")											# <--現在の日付

#####パス定義#####
$WO = "C:\work"																	# <--workフォルダ FMMAXからエクスポートしたCSVファイルを置く場所
$UF = $WO + "\" + "UsedFile"													# <--UsedFileフォルダ 使用したCSVファイルの退避先
$CP = "C:\Program Files (x86)\salesforce.com\Data Loader\cliq_process"			# <--cliq_processフォルダ
$ED = $CP + "\" + "export_DailyAccount"											# <--EXPORTバッチのフォルダのパス
$ID = $CP + "\" + "insert_DailyAccount"											# <--INSERTバッチのフォルダのパス
$DD = $CP + "\" + "delete_DailyAccount"											# <--DELETEバッチのフォルダのパス
$WR = "write"																	# <--バッチのフォルダの中のwriteフォルダ
$RE = "read"																	# <--バッチのフォルダの中のreadフォルダ

#####ファイル定義#####
# CSVファイル
$UD_CSV_FILE = "UTKPR75_日計.csv"												# <--FMMAXからエクスポートしたCSVファイル
$ED_CSV_FILE = "export_DailyAccount.csv"										# <--EXPORTされたファイル
$ID_CSV_FILE = "insert_DailyAccount.csv"										# <--INSERT用CSV
$DD_CSV_FILE = "delete_DailyAccount.csv"										# <--DELETE用CSV

# 絶対パス付SFへインポートするCSVファイル
$UD_CSV = $WO + "\" + $UD_CSV_FILE												# <--FMMAXからエクスポートしたCSVファイル
$ED_CSV = $ED + "\" + $WR + "\" + $ED_CSV_FILE									# <--EXPORTされたファイル					 cliq_process配下
$ID_CSV = $ID + "\" + $RE + "\" + $ID_CSV_FILE									# <--INSERT用CSV 							 cliq_process配下
$DD_CSV = $DD + "\" + $RE + "\" + $DD_CSV_FILE									# <--DELETE用CSV 							 cliq_process配下

# 使用後CSVファイルの絶対パス
$UD_CSV_USED = $UF + "\" + $day + $UD_CSV_FILE 							# <--使用後の加工前のINSERT用csv			UsedFile配下
$ID_CSV_USED = $UF + "\" + $day + $ID_CSV_FILE 							# <--使用後のINSERT用csv 					UsedFile配下
$DD_CSV_USED = $UF + "\" + $day + $DD_CSV_FILE							# <--使用後のDELETE用csv（EXPORTされたcsv） UsedFile配下


#####その他定数#####
# ヘッダー行の文字列
$headerstr = "1,DepartmentCode__c,DepartmentName__c,2,RepCode__c,ThePersonCharge__c,3,OnTheDayDailyTotalSalesAmount__c,OnTheDayDailyTotalSalesLiterNumbe__c,4,5,OnTheDayGrossProfit__c,6,OnTheDayLUnitPrice__c,MonthSalesTotalAmountOfMoney__c,TheCurrentMonthCumulativeL__c,7,8,TheCurrentMonthCumulativeTotalGross__c,MonthBudgetAmount__c,MonthBudgetL__c,9,PreviousYearOnTheSameDaySalesAmount__c,ThePreviousYearOnTheSameDayL__c,PreviousYearOnTheSameDayCumulativ__c,PreviousYearOnTheSameDayCumulative__c,10,11,CommodityClassificationCode__c,CommodityClassification__c,12,TheTargetDate__c"

# --------ここから処理---------

try{
#ファイルの存在確認
If(-not(Test-Path -path $UD_CSV)){
    $wsobj = new-object -comobject wscript.shell
    $result = $wsobj.popup("ファイルが存在しません")
	exit
}
# 1.INSERT用CSVの作成
# ヘッダー行を配列にする

$header = $headerstr -split ","
# 加工前のINSERT用のcsvにヘッダー行を追加したデータベースをcsvとして格納
$before_iCSV = Get-Content $UD_CSV | ConvertFrom-Csv -Header $header

# csvから必要な列のみを抽出したものを格納
$iCSV = $before_iCSV | select "DepartmentCode__c","DepartmentName__c","RepCode__c","ThePersonCharge__c","OnTheDayDailyTotalSalesAmount__c","OnTheDayDailyTotalSalesLiterNumbe__c","OnTheDayGrossProfit__c","OnTheDayLUnitPrice__c","MonthSalesTotalAmountOfMoney__c","TheCurrentMonthCumulativeL__c","TheCurrentMonthCumulativeTotalGross__c","MonthBudgetAmount__c","MonthBudgetL__c","PreviousYearOnTheSameDaySalesAmount__c","ThePreviousYearOnTheSameDayL__c","PreviousYearOnTheSameDayCumulativ__c","PreviousYearOnTheSameDayCumulative__c","CommodityClassificationCode__c","CommodityClassification__c","TheTargetDate__c"

# INSERT用csv表示（デバッグ用）
#$iCSV | ft -AutoSize

# INSERT用csvを出力
$iCSV | Export-Csv $ID_CSV -notype -encoding Default


# 2.EXPORT
# EXPORTバッチのあるフォルダに移動
cd $ED
# EXPORTバッチの実行
.\export_DailyAccount.bat

# 3.INSERT
# INSERTバッチのあるフォルダに移動
cd $ID
# INSERTバッチの実行
.\insert_DailyAccount.bat

# 4.DELETE用csvを配置
# DELETEバッチのreadフォルダにEXPORTバッチで出力されたcsvを移動
Move-Item $ED_CSV $DD_CSV -Force

# 5.DELETE
# DELETEバッチのあるフォルダに移動
cd $DD
# DELETEバッチの実行
.\delete_DailyAccount.bat

# 6.使用したファイルリネームし別のフォルダに退避
# 加工前のinsert用csvファイルを移動
Move-Item $UD_CSV $UD_CSV_USED -Force

# INSERT用csvを移動
Move-Item $ID_CSV $ID_CSV_USED -Force

# Delete用csv（EXPORTされた）を移動
Move-Item $DD_CSV $DD_CSV_USED -Force

}catch [Exception]{
    $msg = "・バッチスクリプト内で例外エラー発生`r`n管理者へ連絡してください`r`n" + $_.Exception.ToString()
    $errfile = $WO + "\" + "Error.txt"
    $msg | Out-File $errfile -Append
    $wsobj = new-object -comobject wscript.shell
    $result = $wsobj.popup($msg)
}