<#
一ノ蔵様　データ連携バッチ
取引先、商品、日計実績トランをOracleからSFへUpsertする

#>


#プロセスbatファイルパス
$ProcessBatchFile = "C:\work\Data Loader\bin"
#DB設定ファイルのパス
$DBSettingFiles = "C:\work\autorelation"

#####パス定義#####
$WO = "C:\work"	

<#
エラーファイル存在確認
0:チェックOK、1:チェックNG
#>
function checkErrFile($filesize){
$execdate = Get-Date -Format "MMdd"
$execdate = "error"+$execdate + "*"
$execPath = $WO + "\status\*.*"
$errorfile = Get-ChildItem $execPath -include $execdate -Recurse | Where-Object {$_.Length -ge $filesize}  | Sort-Object Name -Descending  | Select-Object Name, Length -First 1
Write-Host $errorfile
if($errorfile.Length -eq 0 ){
	return 0
}else{
 	return 1
}


}

<#
エラー発生時のSlack通知
#>
function slack_notification($message) {
 $enc = [System.Text.Encoding]::GetEncoding('ISO-8859-1')
$utf8Bytes = [System.Text.Encoding]::UTF8.GetBytes($message)
 $payload = @{ 
    text = $enc.GetString($utf8Bytes);
    username = "Ichinokura Batch Watching"; 
    icon_emoji = ":frog:"
 }

 Invoke-RestMethod -Uri "https://hooks.slack.com/services/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"  -Method Pos  -Body (ConvertTo-Json $payload)
}


try{
#process.batに移動
cd $ProcessBatchFile

#取引先インポート
Start-Process -FilePath process.bat -ErrorAction Stop -argumentlist $DBSettingFiles,"accountInsert" -Wait
$checkProduct = checkErrFile("650")
if($checkProduct -eq 1){
    #slackに通知
	slack_notification("accountInsertにてエラー")
}

#商品インポート
Start-Process -FilePath process.bat -ErrorAction Stop -argumentlist $DBSettingFiles,"productInsert"  -Wait
$checkProduct = checkErrFile("650")
if($checkProduct -eq 1){
    #slackに通知
	slack_notification("productInsertにてエラー")
}
#日計実績トランインポート
Start-Process -FilePath process.bat -ErrorAction Stop -argumentlist $DBSettingFiles,"dailyTotalInsert" -Wait
$checkProduct = checkErrFile("650")
if($checkProduct -eq 1){
    #slackに通知
	slack_notification("dailyTotalInsertにてエラー")
}

}catch [Exception]{
    #slackに通知
    slack_notification("BATCH ERR dailybatch" + $_.Exception.ToString())
}

  